package com.ait.applied_maths.shunting_yard;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped

public class ShuntingYard {
	
	String infix = "3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3";
	static String theInfix = "";
	static String postfix;
	static Stack<Token> operatorsStack;
	static ArrayList<EvaluationDetails> evaluationDetails = new ArrayList<EvaluationDetails>();  // used to populate the JSF Datatable for the detailed Postfix Evaluation
	static ArrayList<EvaluationDetails> postfixEvaluation = new ArrayList<EvaluationDetails>();
	static boolean showResults;
		
	public static String getPostfix() {
		return postfix;
	}

	public static Stack<Token> getOperatorsStack() {
		return operatorsStack;
	}

	public static ArrayList<EvaluationDetails> getEvaluationDetails() {
		return evaluationDetails;
	}
			
	public static boolean isShowResults() {
		return showResults;
	}

	public static void setShowResults(boolean showResults) {
		ShuntingYard.showResults = showResults;
	}

	public String getInfix() {		
		return infix;
	}	

	public void setInfix(String infix) {
		theInfix = infix;
		this.infix = infix;
	}

	public static void setPostfix(String postfix) {
		ShuntingYard.postfix = postfix;
	}
	
	public static ArrayList<EvaluationDetails> getPostfixEvaluation() {
		return postfixEvaluation;
	}

	public static void setPostfixEvaluation(ArrayList<EvaluationDetails> postfixEvaluation) {
		ShuntingYard.postfixEvaluation = postfixEvaluation;
	}

	public static void cleanupDetailsArray() { // this method formats the operator stack for the JSF Datatable
		for(int i=0; i<evaluationDetails.size(); i++) {

			String[] myStrArr = null;
			if(!evaluationDetails.get(i).getOperatorStack().equals(null)) {
				myStrArr = evaluationDetails.get(i).getOperatorStack().split(",");
				for(int j=0; j<myStrArr.length; j++) {
					if(myStrArr[j].contains("PLUS")) {
						myStrArr[j] = myStrArr[j].replace("PLUS", "+");
					}	
					if(myStrArr[j].contains("MINUS")) {
						myStrArr[j] = myStrArr[j].replace("MINUS", "-");
					}	
					if(myStrArr[j].contains("DIVIDE")) {
						myStrArr[j] = myStrArr[j].replace("DIVIDE", "/");
					}
					if(myStrArr[j].contains("MULTIPLY")) {
						myStrArr[j] = myStrArr[j].replace("MULTIPLY", "*");
					}	
					if(myStrArr[j].contains("POWER")) {
						myStrArr[j] = myStrArr[j].replace("POWER", "^");
					}	
					if(myStrArr[j].contains("LEFTBRACKET")) {
						myStrArr[j] = myStrArr[j].replace("LEFTBRACKET", "(");
					}	
					if(myStrArr[j].contains("RIGHTBRACKET")) {
						myStrArr[j] = myStrArr[j].replace("RIGHTBRACKET", ")");
					}	
					if(myStrArr[j].contains("EQUALS")) {
						myStrArr[j] = myStrArr[j].replace("EQUALS", "=");
					}	
					if(myStrArr[j].contains("[")) {
						myStrArr[j] = myStrArr[j].replace("[", "");
					}
					if(myStrArr[j].contains("]")) {
						myStrArr[j] = myStrArr[j].replace("]", "");
					}
				}
			}
			evaluationDetails.get(i).setOperatorStack(Arrays.toString(myStrArr));
		}
	}
	
	public static ArrayList<EvaluationDetails> getPostfixResults() { // returns the determined Postfix
		evaluationDetails = new ArrayList<EvaluationDetails>(); //reset the ArrayList
		postfixEvaluation = new ArrayList<EvaluationDetails>(); //reset the ArrayList
		String input;
		input = ShuntingYard.theInfix;
		input = input.replaceAll(" ", ""); // remove whitespaces from the input String
		char[] infix = input.toCharArray();	// convert the input String to a char array
		System.out.println("The Infix is : " + input); // print the Infix to the console		
		System.out.println(evaluatePostfix(getReversePolishNotation(infix)));
		cleanupDetailsArray();
		return evaluationDetails;		
	}
	
	public static ArrayList<EvaluationDetails> getThePostfixEvaluation() {		
		showResults = true;
		return postfixEvaluation;		
	}
	
	public static void printState() {
		printStack();
		printPostfix();
	}
	
	public static void printStack() {
		System.out.println("Stack State : " + operatorsStack);
	}
	
	public static void printPostfix() {
		System.out.println("Postfix State : " + postfix);
		System.out.println();
	}	
	
	enum Token {
		PLUS(2, "+", "left", true) {
			@Override
			public double calc(BigDecimal a, BigDecimal b) {
				return (a.add(b)).doubleValue();
			}
		},
		MINUS(2, "-", "left", true) {
			@Override
			public double calc(BigDecimal a, BigDecimal b) {
				return (a.subtract(b)).doubleValue();
			}
		},
		DIVIDE(3, "/", "left", true) {
			@Override
			public double calc(BigDecimal a, BigDecimal b) {
				return (a.divide(b, 2, RoundingMode.HALF_UP)).doubleValue();
			}
		},
		MULTIPLY(3, "*", "left", true) {
			@Override
			public double calc(BigDecimal a, BigDecimal b) {
				return (a.multiply(b)).doubleValue();
			}
		},		
		POWER(4, "^", "right", true) {
			@Override
			public double calc(BigDecimal a, BigDecimal b) {
				int n = new Integer(b.intValue());
				return (a.pow(n)).doubleValue();
			}
		},
		LEFTBRACKET("(", true), // the higher the number, the higher the presidence
		RIGHTBRACKET(")", true),
		EQUALS("=", true),
		OPERAND(111, false);
		
		int presidence;
		double value;
		String symbol;
		boolean isOperator;
		String associativity;
		
		Token() {
		}
		
		Token(String symbol, boolean isOperator) {
			this.symbol = symbol;
			this.isOperator = isOperator;
		}		
		
		Token(int pres, boolean isOperator) {
			this.presidence = pres;
			this.isOperator = isOperator;
		}
		
		Token(int pres, String symbol, boolean isOperator) {
			this.presidence = pres;
			this.symbol = symbol;
		}	
		
		Token(int pres, String symbol, String association, boolean isOperator) {
			this.presidence = pres;
			this.symbol = symbol;
			this.associativity = association;
		}	
		
		public double calc(BigDecimal a, BigDecimal b) {
			// TODO Auto-generated method stub
			return 0.0;
		}
		
		Token(int aValue) {
			this.value = aValue;
		}
		
		public int getPresidence() {
			return presidence;
		}

		public String getSymbol() {
			return symbol;
		}

		public boolean isOperator() {
			return this.isOperator;
		}

		public String getAssociation() {
			return this.associativity;
		}

		public double getValue() {
			return this.value;
		}	
		
		public void setValue(double aValue) {
			this.value = aValue;
		}
	}
	
	public static Token checkToken(char aToken) { // checks the current token, determines if it is an Operator or Operand
		switch(aToken) {
			case '(' : {
				System.out.println("Operator Detected! : leftBracket - Presidence : "+Token.LEFTBRACKET.getPresidence());
				return Token.LEFTBRACKET;
			}
			case ')' : {
				System.out.println("Operator Detected! : rightBracket - Presidence : "+Token.RIGHTBRACKET.getPresidence());
				return Token.RIGHTBRACKET;
			}
			case '+' : {
				System.out.println("Operator Detected! : plus - Presidence : "+Token.PLUS.getPresidence());
				return Token.PLUS;
			}
			case '-' : {
				System.out.println("Operator Detected! : minus - Presidence : "+Token.MINUS.getPresidence());
				return Token.MINUS;
			}
			case '/' : {
				System.out.println("Operator Detected! : divide - Presidence : "+Token.DIVIDE.getPresidence());
				return Token.DIVIDE;
			}
			case '^' : {	
				System.out.println("Operator Detected! : power - Presidence : "+Token.POWER.getPresidence());
				return Token.POWER;
			}
			case '*' : {
				System.out.println("Operator Detected! : multiply - Presidence : "+Token.MULTIPLY.getPresidence());
				return Token.MULTIPLY;
			}
			case '=' : {
				System.out.println("Operator Detected! : equals - Presidence : "+Token.EQUALS.getPresidence());
				return Token.EQUALS;
			}
			default : {
				System.out.println("Operand  Detected! : " + aToken );
				Token.OPERAND.setValue(Character.getNumericValue(aToken));
				return Token.OPERAND;
			}
		}
	}
	
	public static String getReversePolishNotation(char[] infix) {		
		operatorsStack = new Stack<Token>();
		postfix = ""; // Note to self : Change this to either a Queue or an array
		
		System.out.println("\n===========================");
		System.out.println("Infix to Postfix Conversion");
		System.out.println("===========================");
		System.out.println("Begin reading Tokens...\n");
		for(int i=0; i<infix.length; i++) { // while there are tokens to be read
			Token currentToken = checkToken(infix[i]); //read the next token
			myLabel : // used to break out of the while loop when we find 2 instances of right associated operators
			if(currentToken.getPresidence() != 111) { // if the token is an operator
				if(operatorsStack.isEmpty()) { // if the operator stack is empty, push the operator to the stack 
					operatorsStack.push(currentToken); printState(); 
					evaluationDetails.add(new EvaluationDetails(currentToken.getSymbol(), "Push token to stack", postfix, operatorsStack.toString(), "push the operator to the stack"));
				} else if(currentToken.getSymbol().equals("(")) { // if the token is a left bracket (i.e. "("), then:
					operatorsStack.push(currentToken); printState(); // push it onto the operator stack. 
					evaluationDetails.add(new EvaluationDetails(currentToken.getSymbol(), "Push token to stack", postfix, operatorsStack.toString(), ""));
				} else if(!operatorsStack.isEmpty() && currentToken.getSymbol().equals(")")) { // if the token is a right bracket (i.e. ")"), then:	
					while(!operatorsStack.isEmpty() && !operatorsStack.peek().getSymbol().equals("(")) { // while the operator at the top of the stack is not a left bracket:
						postfix = postfix + operatorsStack.pop().getSymbol().toString(); printState(); // pop operators from the stack onto the output queue until we detect a left bracket
						evaluationDetails.add(new EvaluationDetails(String.valueOf(currentToken.getSymbol()), "Pop stack to output", postfix, operatorsStack.toString(), "Repeated until \"(\" found"));
					}
					if(!operatorsStack.isEmpty()) {
						operatorsStack.pop(); printState(); // pop the associated left bracket from the stack.
						evaluationDetails.add(new EvaluationDetails(currentToken.getSymbol(), "Pop stack", postfix, operatorsStack.toString(), "Discard matching parenthesis"));
					}
				} else if(!operatorsStack.isEmpty()) {
					if(currentToken.getSymbol().equals("(") && operatorsStack.peek().getSymbol().equals("(") 
						|| (!currentToken.getSymbol().equals("("))){						
						while(!operatorsStack.isEmpty() && !currentToken.getSymbol().equals("(") 
							&& !currentToken.getSymbol().equals(")") 
								&& (operatorsStack.peek().getPresidence() >= currentToken.getPresidence())) {
							if((operatorsStack.peek().getPresidence() == currentToken.getPresidence()) 
								&& (operatorsStack.peek().getAssociation().equals("right")  
									&& currentToken.getAssociation().equals("right") )) {
								System.out.println("Two instances of Right Associated Operators Detected");
								operatorsStack.push(currentToken);	printState();
								evaluationDetails.add(new EvaluationDetails(currentToken.getSymbol(), "Push token to stack", postfix, operatorsStack.toString(), "^ is evaluated right-to-left"));
								break myLabel;
							} else if(operatorsStack.peek().getPresidence() == currentToken.getPresidence()) { 
								String theSymbol = operatorsStack.peek().getSymbol();
								postfix = postfix + operatorsStack.pop().getSymbol().toString(); 
								//operatorsStack.push(currentToken);	printState(); // here
								evaluationDetails.add(new EvaluationDetails(String.valueOf(currentToken.getSymbol()), "Add token to output", postfix, operatorsStack.toString(), theSymbol + " and " + currentToken.getSymbol() + " have the same presidence"));
							} else if(operatorsStack.peek().getPresidence() > currentToken.getPresidence()) {
								String theSymbol = operatorsStack.peek().getSymbol();
								postfix = postfix + operatorsStack.pop().getSymbol().toString(); printState();
								evaluationDetails.add(new EvaluationDetails(String.valueOf(currentToken.getSymbol()), "Add token to output", postfix, operatorsStack.toString(), theSymbol + " has higher presidence than " + currentToken.getSymbol()));
							}
						}
						//if(!operatorsStack.isEmpty()) {						
						operatorsStack.push(currentToken); printState();
						String theSymbol = operatorsStack.peek().getSymbol();
						evaluationDetails.add(new EvaluationDetails(currentToken.getSymbol(), "Push token to stack", postfix, operatorsStack.toString(), "push the operator to the stack"));
						//}
					} else if(operatorsStack.peek().getPresidence() < currentToken.getPresidence()) {						
						operatorsStack.push(currentToken); printState();
						String theSymbol = operatorsStack.peek().getSymbol(); // here
						evaluationDetails.add(new EvaluationDetails(currentToken.getSymbol(), "Push token to stack", postfix, operatorsStack.toString(), theSymbol + " has higher presidence than " + currentToken.getSymbol()));
					}
				}
			} else if(currentToken.getPresidence() == 111){ // if the token is not an operator
				postfix = postfix + infix[i]; printState(); // add it to the output stream
				evaluationDetails.add(new EvaluationDetails(String.valueOf(infix[i]), "Add token to output", postfix, operatorsStack.toString(), ""));
			}			
		} // end of for-loop
		
		// if there are no more tokens to read:
		while(!operatorsStack.isEmpty()) { // while there are still operator tokens on the stack:
			postfix = postfix + operatorsStack.pop().getSymbol().toString(); // pop the operator onto the output queue 
			printState();
		}		
		evaluationDetails.add(new EvaluationDetails("end", "Pop entire stack to output", postfix,"", ""));
		System.out.println(evaluationDetails);
		return postfix;
	}
	
	public static String evaluatePostfix(String aPostfix) {		
		char[] postfix = aPostfix.toCharArray();
		Stack<String> theStack = new Stack<String>();
		BigDecimal leftOperand, rightOperand;
		
		System.out.println("===========================");
		System.out.println("   PostFix Evaluation");
		System.out.println("===========================\n");
		
		for(int i=0; i<postfix.length; i++) {
			Token currentToken = checkToken(postfix[i]);
			if(currentToken.getPresidence() == 111) {
				theStack.push(String.valueOf(currentToken.getValue())); System.out.println(theStack);
				postfixEvaluation.add(new EvaluationDetails(String.valueOf(currentToken.getValue()), "Push operand to the top of the stack", String.valueOf(postfix), theStack.toString(), "Operand " + currentToken.getValue() + " is pushed to the stack"));
			} else {
				String a = theStack.pop().toString();
				rightOperand = new BigDecimal(a);
				String b = theStack.pop().toString();
				leftOperand = new BigDecimal(b);
				currentToken.setValue(currentToken.calc(leftOperand, rightOperand));
				theStack.push(String.valueOf(currentToken.getValue())); 
				postfixEvaluation.add(new EvaluationDetails(String.valueOf(currentToken.getSymbol()), "Apply operator to the top of the stack", String.valueOf(postfix), theStack.toString(), b + " " + currentToken.getSymbol() + " " + a + " is evaluated & pushed to the stack"));
				System.out.println(theStack);
			}
		}
		postfixEvaluation.add(new EvaluationDetails("end", "The stack now contains the evaluated result", String.valueOf(postfix), theStack.toString(), ""));
		System.out.println(postfixEvaluation);
		return "\nThe Evaluated postfix is : " + theStack.toString();
	}
	
	public static void main(String[] args) {
		String input;
		input = "((5 * 8) +6) / (2-6) * 2";
		input = input.replaceAll(" ", ""); // remove whitespaces from the input String
		char[] infix = input.toCharArray();	// convert the input String to a char array
		System.out.println("The Infix is : " + input); // print the Infix to the console		
		System.out.println(evaluatePostfix(getReversePolishNotation(infix)));
		cleanupDetailsArray();		
	}
}

// To check results : http://www.mathblog.dk/tools/infix-postfix-converter/
// To Evaluate expressions https://www.mathpapa.com/algebra-calculator.html?utm_expid=69051716-60.X1xjqtWkR96Qa_QOTujj4w.0&utm_referrer=https%3A%2F%2Fwww.google.ie%2F
