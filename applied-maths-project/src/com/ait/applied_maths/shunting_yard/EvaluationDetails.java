package com.ait.applied_maths.shunting_yard;

public class EvaluationDetails {
	String token;
	String action;
	String output;
	String operatorStack;
	String notes;	
	
	public EvaluationDetails(String token, String action, String output, String operatorStack, String notes) {
		super();
		this.token = token;
		this.action = action;
		this.output = output;
		this.operatorStack = operatorStack;
		this.notes = notes;
	}
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getOutput() {
		return output;
	}
	public void setOutput(String output) {
		this.output = output;
	}
	public String getOperatorStack() {
		return operatorStack;
	}
	public void setOperatorStack(String operatorStack) {
		this.operatorStack = operatorStack;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public String toString() {
		return "EvaluationDetails [token=" + token + ", action=" + action + ", output=" + output + ", operatorStack="
				+ operatorStack + ", notes=" + notes + "]\n";
	}	
}
