package com.ait.applied_maths.sets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The class {@code Sets} is used for mathematical calculations on sets
 * 
 * @author Team: Athena
 *
 */
public class Sets {

	/**
	 * Adds items to a list from an input string
	 * 
	 * @param string
	 * @return sortedList
	 */
	public ArrayList<Integer> addItemsToList(String numbers) {
		String[] items = numbers.split(" ");

		ArrayList<Integer> inputInt = new ArrayList<>();
		for (int i = 0; i < items.length; i++) {
			inputInt.add(Integer.parseInt(items[i]));
		}
		return inputInt;
	}

	/**
	 * Sorts the items of a list into ascending order
	 * 
	 * @param list
	 * @return sortedList
	 */
	public ArrayList<Integer> sorted(ArrayList<Integer> list) {
		ArrayList<Integer> sortedInteger = new ArrayList<>();

		for (Integer item : list) {
			sortedInteger.add((Integer) item);
		}

		Collections.sort(sortedInteger);
		return sortedInteger;
	}

	/**
	 * Finds the duplicated items in a list
	 * 
	 * @param list
	 * @return set of duplicated values
	 */
	public Set<Integer> findDuplicates(Collection<Integer> list) {
		Set<Integer> uniques = new HashSet<Integer>();

		return list.stream().filter(item -> !uniques.add(item)).collect(Collectors.toSet());
	}

	/**
	 * Finds the unique items in a list
	 * 
	 * @param originalList
	 * @param dublicatedValues
	 * @return uniqueValues
	 */
	public ArrayList<Integer> findUniques(ArrayList<Integer> original, Set<Integer> duplicates) {
		original.removeAll(duplicates);
		return original;
	}

	/**
	 * Calculates the total of a list
	 * 
	 * @param originalList
	 * @return total
	 */
	public Integer total(ArrayList<Integer> original) {
		int total = 0;
		for (Integer number : original) {
			total += number;
		}
		return total;
	}

	/**
	 * Calculates the mean(average) of a list
	 * 
	 * @param originalList
	 * @return total/count
	 */
	public Double mean(ArrayList<Integer> original) {
		int total = 0;
		int count = 0;
		for (int number : original) {
			total += number;
			count++;
		}
		return (double) total / count;
	}

	/**
	 * Calculates the median of a list
	 * 
	 * @param originalList
	 * @return median value
	 */
	public Double median(ArrayList<Integer> sortedList) {
		int middle = sortedList.size() / 2;
		if (sortedList.size()%2 == 1) {
			return (double) sortedList.get(middle);

		} else {
			return ((double) sortedList.get(middle - 1) + (double) sortedList.get(middle)) / 2.0;
		}
	}

	/**
	 * Calculates the range of a list
	 * 
	 * @param originalList
	 * @return range
	 */
	public String range(ArrayList<Integer> original) {

		int firstElement = original.get(0);
		int max = firstElement;
		int min = firstElement;

		for (Integer num : original) {
			max = Math.max(max, num);
			min = Math.min(min, num);

		}
		return "Range " + min + " - " + max;
	}

	/**
	 * Calculates the mode (most frequent value) of a list
	 * 
	 * @param originalList
	 * @return mode
	 */
	public String mode(ArrayList<Integer> original) {
		int maxValue = 0;
		int maxCount = 0;

		for (int i = 0; i < original.size(); ++i) {
			int count = 0;
			for (int j = 0; j < original.size(); ++j) {
				if (original.get(j) == original.get(i))
					++count;
			}
			if (count > maxCount) {
				maxCount = count;
				maxValue = original.get(i);
			}
		}

		return "Value: " + maxValue + " Frequency: " + maxCount;
	}

	/**
	 * Writes out the result
	 * 
	 * @param originalList
	 * @return output
	 */
	public String writeOut(Collection<Integer> list) {
		String outPut = "";
		for (Integer item : list) {
			outPut += item + " ";
		}
		return outPut;
	}

}
