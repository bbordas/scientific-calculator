package com.ait.mase.applied_maths.models;

import java.io.Serializable;

/**
 * POJO class representing the values used for Root to the nth degree functions
 * 
 * @author Team: Athena
 *
 */
public class RootToNthDegree implements Serializable {

	private static final long serialVersionUID = 1L;

	private double rootValue;

	private double ofValue;

	private double answer = 0;

	public double getRootValue() {
		return rootValue;
	}

	public void setRootValue(double rootValue) {
		this.rootValue = rootValue;
	}

	public double getOfValue() {
		return ofValue;
	}

	public void setOfValue(double ofValue) {
		this.ofValue = ofValue;
	}

	public double getAnswer() {
		return answer;
	}

	public void setAnswer(double answer) {
		this.answer = answer;
	}

}
