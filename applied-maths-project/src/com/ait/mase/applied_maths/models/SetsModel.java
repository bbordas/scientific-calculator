package com.ait.mase.applied_maths.models;

import java.io.Serializable;

/**
 * POJO class representing the values used for set functions
 * 
 * @author Team: Athena
 *
 */

public class SetsModel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String input;
	private String outPut;
	
	public String getOutPut() {
		return outPut;
	}
	public void setOutPut(String outPut) {
		this.outPut = outPut;
	}
	public String getInput() {
		return input;
	}
	public void setInput(String input) {
		this.input = input;
	}
}
