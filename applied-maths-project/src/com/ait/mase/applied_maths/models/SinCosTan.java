package com.ait.mase.applied_maths.models;

import java.io.Serializable;


public class SinCosTan implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private double value;

	private String baseType;
	
	private String RadDeg;

	private double answer;

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getRadDeg() {
		return RadDeg;
	}

	public void setRadDeg(String RadDeg) {
		this.RadDeg = RadDeg;
	}
	
	public String getBaseType() {
		return baseType;
	}

	public void setBaseType(String baseType) {
		this.baseType = baseType;
	}


	public double getAnswer() {
		return answer;
	}

	public void setAnswer(double answer) {
		this.answer = answer;
	}
	
}
