package com.ait.mase.applied_maths.models;

import java.io.Serializable;

/**
 * POJO class representing the values used for probability combinations
 * 
 * @author Team: Athena
 *
 */

public class Combination implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private double blackBalls;
	
	private double whiteBalls;
	
	private double n;

	private double r;

	private double answer;
	
	private String nType;
	
	public String getnType() {
		return nType;
	}

	public void setnType(String nType) {
		this.nType = nType;
	}

	public double getBlackBalls() {
		return blackBalls;
	}

	public void setBlackBalls(double blackBalls) {
		this.blackBalls = blackBalls;
	}

	public double getWhiteBalls() {
		return whiteBalls;
	}

	public void setWhiteBalls(double whiteBalls) {
		this.whiteBalls = whiteBalls;
	}

	public double getN() {
		return n;
	}

	public void setN(double n) {
		this.n = n;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public double getAnswer() {
		return answer;
	}

	public void setAnswer(double answer) {
		this.answer = answer;
	}

	

}
