package com.ait.mase.applied_maths.models;

import java.io.Serializable;

/**
 * POJO class representing the values used for Probability functions
 * 
 * @author Team: Athena
 *
 */


public class Probability implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private double numPossibleOutcomes;

	private double numPositiveOutcomes;

	private double answer;

	public double getNumPossibleOutcomes() {
		return numPossibleOutcomes;
	}

	public void setNumPossibleOutcomes(double numPossibleOutcomes) {
		this.numPossibleOutcomes = numPossibleOutcomes;
	}

	public double getNumPositiveOutcomes() {
		return numPositiveOutcomes;
	}

	public void setNumPositiveOutcomes(double numPositiveOutcomes) {
		this.numPositiveOutcomes = numPositiveOutcomes;
	}

	public double getAnswer() {
		return answer;
	}

	public void setAnswer(double answer) {
		this.answer = answer;
	}
	
	

}
