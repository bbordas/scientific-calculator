package com.ait.mase.applied_maths.models;

import java.io.Serializable;

/**
 * POJO class representing the values used for calculate sum functions
 * 
 * @author Team: Athena
 *
 */

public class BasicCalculations implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private double value1;
	
	private double value2;
	
	private String type;
	
	private String degRad;
	
	private double rad;
	
	private double answer;
	
	
	public double getValue1() {
		return value1;
	}
	public void setValue1(double value1) {
		this.value1 = value1;
	}
	public double getValue2() {
		return value2;
	}
	public void setValue2(double value2) {
		this.value2 = value2;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDegRad() {
		return degRad;
	}
	public void setDegRad(String degRad) {
		this.degRad = degRad;
	}
	public double getAnswer() {
		return answer;
	}
	public void setAnswer(double answer) {
		this.answer = answer;
	}
	public double getRad() {
		return rad;
	}
	public void setRad(double rad) {
		this.rad = rad;
	}

}
