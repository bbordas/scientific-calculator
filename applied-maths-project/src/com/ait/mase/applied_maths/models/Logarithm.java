package com.ait.mase.applied_maths.models;

import java.io.Serializable;

/**
 * POJO class representing the values used for Logarithm functions
 * 
 * @author Team: Athena
 *
 */
public class Logarithm implements Serializable {

	private static final long serialVersionUID = 1L;

	private double value;

	private String baseType;

	private double answer;

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getBaseType() {
		return baseType;
	}

	public void setBaseType(String baseType) {
		this.baseType = baseType;
	}

	public double getAnswer() {
		return answer;
	}

	public void setAnswer(double answer) {
		this.answer = answer;
	}

}
