package com.ait.mase.applied_maths.models;

import java.io.Serializable;

/**
 * POJO class representing the values used for Exponential functions
 * 
 * @author Team: Athena
 *
 */
public class Exponential implements Serializable {

	private static final long serialVersionUID = 1L;

	private double value;

	private double answer;

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public double getAnswer() {
		return answer;
	}

	public void setAnswer(double answer) {
		this.answer = answer;
	}

}
