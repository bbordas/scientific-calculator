package com.ait.mase.applied_maths.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * Bean class used for retrieving and mutating the name of the DWT images found
 * in the path: WebContent/resources/images/DWT
 * 
 * @author A00248298
 *
 */
@ManagedBean
@RequestScoped
public class DWTBean {

	/**
	 * The default name of the compressed image (used to display the initial image
	 * in the front-end when the page loads)
	 */
	private String selectedCompressedImageLocation = "imageCompression D3Q16.bmp";

	/**
	 * The default name of the reconstructed image (used to display the initial
	 * image in the front-end when the page loads)
	 */
	private String selectedReconstructedImageLocation = "imageReconstruction D3Q16.bmp";

	public DWTBean() {

	}

	/**
	 * Set the name of the selectedReconstructedImageLocation depending on the
	 * selected selectedCompressedImageLocation value by replacing "imageCompression" to
	 * "imageReconstruction"
	 */
	public void updateSelectedReconstructedImageLocation() {
		selectedReconstructedImageLocation = selectedCompressedImageLocation.replace("imageCompression",
				"imageReconstruction");
	}

	public String getSelectedCompressedImageLocation() {
		return selectedCompressedImageLocation;
	}

	public void setSelectedCompressedImageLocation(String selectedCompressedImageLocation) {
		this.selectedCompressedImageLocation = selectedCompressedImageLocation;
	}

	public String getSelectedReconstructedImageLocation() {
		return selectedReconstructedImageLocation;
	}

	public void setSelectedReconstructedImageLocation(String selectedReconstructedImageLocation) {
		this.selectedReconstructedImageLocation = selectedReconstructedImageLocation;
	}

}
