package com.ait.mase.applied_maths.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import com.ait.applied_maths.sets.Sets;
import com.ait.mase.applied_maths.models.SetsModel;

/**
 * Java Bean class used to communicate between the server and the view and
 * perform mathematical calculations on sets
 * 
 * @author Team: Athena
 *
 */

@ManagedBean
@RequestScoped
public class SetBean implements Serializable {
	/**
	 * Default serial ID
	 */
	private static final long serialVersionUID = 1L;

	private SetsModel sm = new SetsModel();
	private Sets set = new Sets();

	/**
	 * Sorts the items of a set into ascending order
	 * 
	 * @return the sorted set
	 */
	public String sort() {
		sm.setOutPut(set.writeOut(set.sorted(set.addItemsToList(sm.getInput()))));

		return sm.getOutPut();

	}

	/**
	 * Checks for duplicated values in a set
	 * 
	 * @return the duplicated values
	 */
	public String duplicates() {
		sm.setOutPut(set.writeOut(set.findDuplicates(set.sorted(set.addItemsToList(sm.getInput())))));
		return sm.getOutPut();
	}

	/**
	 * Checks for unique values in a set
	 * 
	 * @return the unique values
	 * 
	 */
	public String unique() {

		sm.setOutPut(set.writeOut(set.findUniques(set.sorted(set.addItemsToList(sm.getInput())),
				set.findDuplicates(set.sorted(set.addItemsToList(sm.getInput()))))));
		return sm.getOutPut();
	}

	/**
	 * Calculates the total of a set
	 * 
	 * @return the total
	 */
	public String total() {
		sm.setOutPut(set.total(set.addItemsToList(sm.getInput())).toString());
		return sm.getOutPut();
	}

	/**
	 * Calculates the mean(average) of a set
	 * 
	 * @return the mean
	 */
	public String mean() {
		sm.setOutPut(set.mean(set.addItemsToList(sm.getInput())).toString());
		return sm.getOutPut();
	}

	/**
	 * Calculates the mode (Most frequent value) of a set
	 * 
	 * @return the most frequent value and the frequency
	 */
	public String mode() {

		sm.setOutPut(set.mode(set.addItemsToList(sm.getInput())));
		return sm.getOutPut();
	}

	/**
	 * Calculates the range of a set
	 * 
	 * @return the range (minimum - maximum)
	 */
	public String range() {
		sm.setOutPut(set.range(set.addItemsToList(sm.getInput())).toString());
		return sm.getOutPut();
	}

	/**
	 * Calculates the median of a set
	 * 
	 * @return the most median
	 */
	public String median() {
		sm.setOutPut(set.median(set.sorted(set.addItemsToList(sm.getInput()))).toString());
		return sm.getOutPut();
	}

	/**
	 * Reset all the set values back to default
	 */
		public void clearAll() {
			set = new Sets();
			sm = new SetsModel();
		}
	
		public SetsModel getSm() {
			return sm;
		}
	
		public void setSm(SetsModel sm) {
			this.sm = sm;
		}
	
		public Sets getSet() {
			return set;
		}
	
		public void setSet(Sets set) {
			this.set = set;
		}
}
