package com.ait.mase.applied_maths.beans;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * Bean for Shunting Yard, used to set and retrieve the infix and result
 * 
 * @author Team: Athena
 *
 */
@ManagedBean
@RequestScoped
public class ShuntingYardBean implements Serializable {

	private static final long serialVersionUID = 1L;
	public boolean showResult;
	String infix = "3 + 4 * 2 / ( 1 - 5 ) ^ 2 ^ 3";

	public boolean isShowResult() {
		return showResult;
	}

	public void setShowResult(boolean showResult) {
		this.showResult = showResult;
	}

	public String getInfix() {
		return infix;
	}

	public void setInfix(String infix) {
		this.infix = infix;
	}

	// setup infix here & pass it to the below method
	// set show results to true
	public void performShuntingYard(String infix) {

	}

}
