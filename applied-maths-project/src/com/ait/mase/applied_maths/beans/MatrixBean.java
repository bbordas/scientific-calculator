package com.ait.mase.applied_maths.beans;

import java.io.Serializable;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.ait.mase.applied_maths.calculations.Matrices;

/**
 * Bean class used to generate and calculate two matrices (e.g. addition,
 * subtraction, multiplication, division), and randomize array values
 * 
 * @author Team: Athena
 *
 */
@ManagedBean
@RequestScoped
public class MatrixBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer[][] basicMatrix;

	private int rows = 4;

	private int columns = 4;

	private int minRandomNumber = 0;

	private int maxRandomNumber = 255;

	private Integer[][] matrixA;

	private Integer[][] matrixB;

	private Object[][] calculationResult;

	private static String matrixRowsAndColumnsSetting = "3*3";

	private String errorMessage;

	/**
	 * Initialization post the constructor. Used to adjust the rows and columns of
	 * the matrices (matrixA and matrixB)
	 */
	@PostConstruct
	public void init() {
		adjustRowsAndColumns();
	}

	/**
	 * Populate the matrices (matrixA and matrixB) with random values
	 */
	public void populateMatrixAandB() {
		Random randomGenerator = new Random();

		for (int r = 0; r < matrixA[0].length; r++) {
			for (int c = 0; c < matrixA[1].length; c++) {
				matrixA[r][c] = randomGenerator.nextInt(255);
				matrixB[r][c] = randomGenerator.nextInt(255);
			}
		}

		calculationResult = null;
	}

	/**
	 * Add, subtract, or divide two matrices (depending on the selected operation)
	 * 
	 * @param operationType
	 *            The selected operation to perform on the matrices
	 */
	public void addSubtractOrDivideMatrices(String operationType) {
		calculationResult = Matrices.addSubtractOrDivideMatrices(matrixA, matrixB, operationType);
	}

	/**
	 * Multiply two matrices
	 */
	public void multiplyMatrices() {
		calculationResult = Matrices.multiplyMatrices(matrixA, matrixB);
	}

	/**
	 * Sets basicMatrix array to random values (depending on the requested
	 * properties). 
	 * An errorMessage is displayed if incorrect properties are
	 * requested (i.e. if the requested minimum number is larger than the maximum
	 * number)
	 * 
	 * @param row
	 *            The number of rows of matrix
	 * @param column
	 *            The number of columns of matrix
	 * @param minRandomNumber
	 *            The minimum random generated number of the matrix
	 * @param maxRandomNumber
	 *            The maximum random generated number of the matrix
	 */
	public void getBasicMatrix(int row, int column, int minRandomNumber, int maxRandomNumber) {
		if (minRandomNumber > maxRandomNumber) {
			this.errorMessage = "Error: Minimum random number cannot be greater than maximum random number.";
			return;
		} else {
			this.errorMessage = "";
			basicMatrix = Matrices.generateBasicMatrix(row, column, minRandomNumber, maxRandomNumber);
		}
	}

	/**
	 * Adjusts the size of the matrices array depending on the value selected in the
	 * drop-down menu in front-end (i.e. the value of matrixRowsAndColumnsSetting
	 * variable) and also resets the calculationResult array.
	 */
	public void adjustRowsAndColumns() {
		if (matrixRowsAndColumnsSetting.equals("3*3")) {
			matrixA = new Integer[3][3];
			matrixB = new Integer[3][3];
		} else if (matrixRowsAndColumnsSetting.equals("4*4")) {
			matrixA = new Integer[4][4];
			matrixB = new Integer[4][4];
		} else if (matrixRowsAndColumnsSetting.equals("5*5")) {
			matrixA = new Integer[5][5];
			matrixB = new Integer[5][5];
		}
		calculationResult = null;
	}

	public Integer[][] getBasicMatrix() {
		return basicMatrix;
	}

	public void setBasicMatrix(Integer[][] basicMatrix) {
		this.basicMatrix = basicMatrix;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public int getMinRandomNumber() {
		return minRandomNumber;
	}

	public void setMinRandomNumber(int minRandomNumber) {
		this.minRandomNumber = minRandomNumber;
	}

	public int getMaxRandomNumber() {
		return maxRandomNumber;
	}

	public void setMaxRandomNumber(int maxRandomNumber) {
		this.maxRandomNumber = maxRandomNumber;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Integer[][] getMatrixA() {
		return matrixA;
	}

	public void setMatrixA(Integer[][] matrixA) {
		this.matrixA = matrixA;
	}

	public Integer[][] getMatrixB() {
		return matrixB;
	}

	public void setMatrixB(Integer[][] matrixB) {
		this.matrixB = matrixB;
	}

	public String getMatrixRowsAndColumnsSetting() {
		return matrixRowsAndColumnsSetting;
	}

	@SuppressWarnings("static-access")
	public void setMatrixRowsAndColumnsSetting(String matrixRowsAndColumnsSetting) {
		this.matrixRowsAndColumnsSetting = matrixRowsAndColumnsSetting;
	}

	public Object[][] getCalculationResult() {
		return calculationResult;
	}

	public void setCalculationResult(Object[][] calculationResult) {
		this.calculationResult = calculationResult;
	}

}
