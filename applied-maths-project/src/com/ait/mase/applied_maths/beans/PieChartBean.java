package com.ait.mase.applied_maths.beans;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.primefaces.model.chart.PieChartModel;

/**
 * Bean class used for generating a pie chart used to display percentage of
 * given values
 * 
 * @author Team: Athena
 *
 */
@ManagedBean
@RequestScoped
public class PieChartBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private PieChartModel pieModel2;

	private String pieTextBoxValue;

	private double average;

	private double total;

	private double max;

	private double min;

	/**
	 * Create a default pie chart when the page loads
	 */
	@PostConstruct
	public void init() {
		createPieModel2();
	}

	public PieChartModel getPieModel2() {
		return pieModel2;
	}

	/**
	 * Default pie chart created when the page loads
	 */
	public void createPieModel2() {
		pieModel2 = new PieChartModel();

		pieModel2.set("Value 1 (54)", 54);
		pieModel2.set("Value 2 (32)", 32);
		pieModel2.set("Value 3 (70)", 70);
		pieModel2.set("Value 4 (42)", 42);

		pieModel2.setTitle("Pie Chart - Percentage");
		pieModel2.setLegendPosition("w");
		pieModel2.setFill(true);
		pieModel2.setShowDataLabels(true);
		pieModel2.setDiameter(200);
	}

	/**
	 * Dynamic pie chart (depending on the values entered by the users input)
	 */
	public void createPieModel3() {
		try {
			pieModel2 = new PieChartModel();
			pieModel2.setTitle("Pie Chart - Percentage");
			pieModel2.setLegendPosition("w");
			pieModel2.setShowDataLabels(true);
			pieModel2.setDiameter(200);

			pieTextBoxValue.replaceAll("\\s+", "");
			List<String> items = Arrays.asList(pieTextBoxValue.split("\\s*,\\s*"));

			int count = 1;

			for (String item : items) {
				double value = Double.parseDouble(item);
				pieModel2.set("Value " + count++ + " (" + value + ")", value);
				total += value;

				if (value > max)
					max = value;

				if (value < min || min == 0)
					min = value;

			}

			average = total / items.size();

		} catch (NumberFormatException nfe) {
			pieModel2.clear();
			pieModel2.set("Error: Invalid Data Format", 1);
			pieModel2.setShowDataLabels(false);
			pieModel2.setTitle("An Error Occurred - Invalid Data Format");
			average = 0;
			total = 0;
			min = 0;
			max = 0;
		}
	}

	public String getPieTextBoxValue() {
		return pieTextBoxValue;
	}

	public void setPieTextBoxValue(String pieTextBoxValue) {
		this.pieTextBoxValue = pieTextBoxValue;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

}
