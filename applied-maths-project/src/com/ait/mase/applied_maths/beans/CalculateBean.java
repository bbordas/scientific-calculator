package com.ait.mase.applied_maths.beans;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.ait.mase.applied_maths.calculations.Calculate;
import com.ait.mase.applied_maths.models.BasicCalculations;
import com.ait.mase.applied_maths.models.Combination;
import com.ait.mase.applied_maths.models.Exponential;
import com.ait.mase.applied_maths.models.Logarithm;
import com.ait.mase.applied_maths.models.Power;
import com.ait.mase.applied_maths.models.Probability;
import com.ait.mase.applied_maths.models.RootToNthDegree;
import com.ait.mase.applied_maths.models.SinCosTan;
import com.ait.mase.applied_maths.models.SquareRoot;

/**
 * Java Bean class used to communicate between the server and the view and
 * perform mathematical calculations
 * 
 * @author Team: Athena
 *
 */
@ManagedBean
@RequestScoped
public class CalculateBean implements Serializable {

	/**
	 * Default serial ID
	 */
	private static final long serialVersionUID = 1L;

	private RootToNthDegree rootToNthDegree = new RootToNthDegree();

	private SquareRoot sqrt = new SquareRoot();

	private Logarithm log = new Logarithm();

	private Exponential exp = new Exponential();
	
	private Probability prob = new Probability();
	
	private Combination comb = new Combination();

	private Power pow = new Power();

	private int roundValue = 4;

	private SinCosTan sct = new SinCosTan();
	
	private SinCosTan sct1 = new SinCosTan();

	private BasicCalculations bCalc = new BasicCalculations();

	/**
	 * Calculates the exponential of a value
	 * 
	 * @return the exponential result
	 */
	public double calcExponential() {
		exp.setAnswer(Calculate.round(Calculate.expOf(exp.getValue()), roundValue));
		return exp.getAnswer();
	}

	/**
	 * Calculate either natural or base 10 logarithm depending on the required
	 * result
	 * 
	 * @return logarithm result
	 */
	public double calcLogarithm() {
		if (log.getBaseType().equalsIgnoreCase("natural"))
			log.setAnswer(Calculate.round(Calculate.lnOf(log.getValue()), roundValue));
		else if (log.getBaseType().equalsIgnoreCase("base10"))
			log.setAnswer(Calculate.round(Calculate.ln10Of(log.getValue()), roundValue));
		return log.getAnswer();
	}
	
	/**
	 * Calculate the probability of an event occurring.
	 * @return probability.
	 */
	public double calcProbability() {
		prob.setAnswer(Calculate.round(Calculate.probability(prob.getNumPossibleOutcomes(), prob.getNumPositiveOutcomes()), 4));
		return prob.getAnswer();
	}
	
	/**
	 * Calculates the probability of a certain combination occurring
	 * @return probability of combinations
	 */
	public double calcCombination() {
		if (comb.getnType().equalsIgnoreCase("white"))
			comb.setN(comb.getWhiteBalls());
			comb.setAnswer(Calculate.round(Calculate.combination(comb.getWhiteBalls(), comb.getBlackBalls(), comb.getN(), comb.getR()), 10));
			
		if(comb.getnType().equalsIgnoreCase("black"))
			comb.setN(comb.getBlackBalls());
			comb.setAnswer(Calculate.round(Calculate.combination(comb.getWhiteBalls(), comb.getBlackBalls(), comb.getN(), comb.getR()), 10));
			
		return comb.getAnswer();
	}

	/**
	 * Calculate the root of a double to the Nth degree
	 * 
	 * @return the root of the double to the Nth degree
	 */
	public double calcRoot() {
		rootToNthDegree.setAnswer(Calculate
				.round(Calculate.rootOf(rootToNthDegree.getRootValue(), rootToNthDegree.getOfValue()), roundValue));
		return rootToNthDegree.getAnswer();
	}

	/**
	 * Calculate the square root of a value
	 * 
	 * @return the square root of a value
	 */
	public double calcSqrRoot() {
		sqrt.setAnswer(Calculate.round(Calculate.sqrRootOf(sqrt.getValue()), roundValue));
		return sqrt.getAnswer();
	}

	/**
	 * Calculate the power of a value
	 * 
	 * @return the power of a value
	 */
	public double calcPower() {
		pow.setAnswer(Calculate.round(Calculate.powerOf(pow.getValue(), pow.getPower()), roundValue));
		return pow.getAnswer();
	}

	/**
	 * Calculate sin, cos, or tan (depending on the required operation)
	 * 
	 * @return the result of the sin/cos/tan
	 */
	public double calcSinCosTan() {
		if (sct.getBaseType().equalsIgnoreCase("Sin"))
			sct.setAnswer(Calculate.round(Calculate.sinof(sct.getValue()), roundValue));
		else if (sct.getBaseType().equalsIgnoreCase("Cos"))
			sct.setAnswer(Calculate.round(Calculate.cosof(sct.getValue()), roundValue));
		else if (sct.getBaseType().equalsIgnoreCase("Tan"))
			sct.setAnswer(Calculate.round(Calculate.tanof(sct.getValue()), roundValue));
		return sct.getAnswer();
	}

	public double calcSinCosTan1() {
		if (sct1.getBaseType().equalsIgnoreCase("Sin"))
			sct1.setAnswer(Calculate.round(Calculate.sinofDeg(sct1.getValue()), roundValue));
		else if (sct1.getBaseType().equalsIgnoreCase("Cos"))
			sct1.setAnswer(Calculate.round(Calculate.cosofDeg(sct1.getValue()), roundValue));
		else if (sct1.getBaseType().equalsIgnoreCase("Tan"))
			sct1.setAnswer(Calculate.round(Calculate.tanofDeg(sct1.getValue()), roundValue));
		return sct1.getAnswer();
	}
	
	/**
	 * Calculate basic operations (i.e. plus, minus, subtract, divide) of two values
	 * (depending on the selected operation from the front-end). Answer can also be
	 * set to radians or degrees
	 * 
	 * @return the result of two values depending on the selected operation
	 *         (presented in radians or degrees)
	 */
	public double calcBasicCalculations() {

		if (bCalc.getType().equalsIgnoreCase("plus")) {
			bCalc.setAnswer(Calculate.round(Calculate.additionOf(bCalc.getValue1(), bCalc.getValue2()), roundValue));
		} else if (bCalc.getType().equalsIgnoreCase("subtract")) {
			bCalc.setAnswer(
					Calculate.round(Calculate.substractionOf(bCalc.getValue1(), bCalc.getValue2()), roundValue));
		} else if (bCalc.getType().equalsIgnoreCase("divide")) {
			if (bCalc.getValue2() != 0) {
				bCalc.setAnswer(
						Calculate.round(Calculate.quotientOf(bCalc.getValue1(), bCalc.getValue2()), roundValue));
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Invalid Operation!", "Dividing by zero is undefined "));
			}

		} else if (bCalc.getType().equalsIgnoreCase("multiply")) {
			bCalc.setAnswer(Calculate.round(Calculate.productOf(bCalc.getValue1(), bCalc.getValue2()), roundValue));
		}

//		if (bCalc.getDegRad().equalsIgnoreCase("radian")) {
//			bCalc.setRad(Calculate.calculateRadian(bCalc.getAnswer()));
//			bCalc.setAnswer(Calculate.round(bCalc.getRad(), roundValue));
//		}

		return bCalc.getAnswer();
	}

	/**
	 * Reset all the set values back to default
	 */
	public void clearAll() {
		rootToNthDegree = new RootToNthDegree();
		sqrt = new SquareRoot();
		log = new Logarithm();
		exp = new Exponential();
		pow = new Power();
		sct = new SinCosTan();
		sct1 = new SinCosTan();
		bCalc = new BasicCalculations();
		sct = new SinCosTan();
		sct1 = new SinCosTan();
	}

	public SquareRoot getSqrt() {
		return sqrt;
	}

	public void setSqrt(SquareRoot sqrt) {
		this.sqrt = sqrt;
	}

	public RootToNthDegree getRootToNthDegree() {
		return rootToNthDegree;
	}

	public void setRootToNthDegree(RootToNthDegree rootToNthDegree) {
		this.rootToNthDegree = rootToNthDegree;
	}

	public Power getPow() {
		return pow;
	}

	public void setPow(Power pow) {
		this.pow = pow;
	}

	public Logarithm getLog() {
		return log;
	}

	public void setLog(Logarithm log) {
		this.log = log;
	}

	public Exponential getExp() {
		return exp;
	}

	public void setExp(Exponential exp) {
		this.exp = exp;
	}

	public int getRoundValue() {
		return roundValue;
	}

	public void setRoundValue(int roundValue) {
		this.roundValue = roundValue;
	}

	public SinCosTan getSct() {
		return sct;
	}

	public void setSct(SinCosTan sct) {
		this.sct = sct;
	}

	public SinCosTan getSct1() {
		return sct1;
	}

	public void setSct1(SinCosTan sct1) {
		this.sct = sct1;
	}
	
	public BasicCalculations getbCalc() {
		return bCalc;
	}

	public void setbCalc(BasicCalculations bCalc) {
		this.bCalc = bCalc;
	}

	public Probability getProb() {
		return prob;
	}

	public void setProb(Probability prob) {
		this.prob = prob;
	}

	public Combination getComb() {
		return comb;
	}

	public void setComb(Combination comb) {
		this.comb = comb;
	}
}