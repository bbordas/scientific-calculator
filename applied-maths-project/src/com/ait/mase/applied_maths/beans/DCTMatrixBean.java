package com.ait.mase.applied_maths.beans;

import java.io.Serializable;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.ait.mase.applied_math.dct.DCT_Driver;
import com.ait.mase.applied_maths.calculations.Matrices;

/**
 * Bean class used to generate and calculate DCT matrix
 * 
 * @author Team: Athena
 *
 */
@ManagedBean
@RequestScoped
public class DCTMatrixBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer[][] basicMatrix;

	private int rows = 8;

	private int columns = 8;

	private int minRandomNumber = 0;

	private int maxRandomNumber = 255;

	private Integer[][] matrixA;

	private Integer[][] matrixB;

	private Object[][] calculationResult;

	private Integer[][] compressedResult;

	private static String matrixRowsAndColumnsSetting = "8*8";

	private String errorMessage;

	private DCT_Driver myDCT_Driver = new DCT_Driver();

	private int quantizationLevel;

	@PostConstruct
	public void init() {
		adjustRowsAndColumns();
	}

	/**
	 * Populate the matrices (matrixA and matrixB) with random values
	 */
	public void populateMatrixAandB() {
		Random randomGenerator = new Random();

		for (int r = 0; r < matrixA[0].length; r++) {
			for (int c = 0; c < matrixA[1].length; c++) {
				matrixA[r][c] = randomGenerator.nextInt(255);
				matrixB[r][c] = randomGenerator.nextInt(255);
			}
		}

		calculationResult = null;
	}

	public int getQuantizationLevel() {
		return quantizationLevel;
	}

	public void setQuantizationLevel(int quantizationLevel) {
		this.quantizationLevel = quantizationLevel;
	}

	public Integer[][] getCompressedResult() {
		compressedResult = myDCT_Driver.getCMatrix();
		return compressedResult;
	}

	public void setCompressedResult(Integer[][] compressedResult) {
		this.compressedResult = compressedResult;
	}

	public void addSubtractOrDivideMatrices(String operationType) {
		calculationResult = Matrices.addSubtractOrDivideMatrices(matrixA, matrixB, operationType);
	}

	public void multiplyMatrices() {
		calculationResult = Matrices.multiplyMatrices(matrixA, matrixB);
	}

	public void applyDCT() {
		calculationResult = myDCT_Driver.compress(quantizationLevel, matrixA);
		if (calculationResult == null)
			System.out.println("null");
		else
			System.out.println("not null");
	}

	public void getBasicMatrix(int row, int column, int minRandomNumber, int maxRandomNumber) {
		if (minRandomNumber > maxRandomNumber) {
			this.errorMessage = "Error: Minimum random number cannot be greater than maximum random number.";
			return;
		} else {
			this.errorMessage = "";
			basicMatrix = Matrices.generateBasicMatrix(row, column, minRandomNumber, maxRandomNumber);
		}
	}

	/**
	 * Adjusts the size of the matrices array depending on the requested value in
	 * front-end and also resets the calculationResult array.
	 */
	public void adjustRowsAndColumns() {
		if (matrixRowsAndColumnsSetting.equals("3*3")) {
			matrixA = new Integer[3][3];
			matrixB = new Integer[3][3];
		} else if (matrixRowsAndColumnsSetting.equals("4*4")) {
			matrixA = new Integer[4][4];
			matrixB = new Integer[4][4];
		} else if (matrixRowsAndColumnsSetting.equals("5*5")) {
			matrixA = new Integer[5][5];
			matrixB = new Integer[5][5];
		} else if (matrixRowsAndColumnsSetting.equals("8*8")) {
			matrixA = new Integer[8][8];
			matrixB = new Integer[8][8];
		}
		calculationResult = null;
	}

	public Integer[][] getBasicMatrix() {
		return basicMatrix;
	}

	public void setBasicMatrix(Integer[][] basicMatrix) {
		this.basicMatrix = basicMatrix;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public int getMinRandomNumber() {
		return minRandomNumber;
	}

	public void setMinRandomNumber(int minRandomNumber) {
		this.minRandomNumber = minRandomNumber;
	}

	public int getMaxRandomNumber() {
		return maxRandomNumber;
	}

	public void setMaxRandomNumber(int maxRandomNumber) {
		this.maxRandomNumber = maxRandomNumber;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Integer[][] getMatrixA() {
		return matrixA;
	}

	public void setMatrixA(Integer[][] matrixA) {
		this.matrixA = matrixA;
	}

	public Integer[][] getMatrixB() {
		return matrixB;
	}

	public void setMatrixB(Integer[][] matrixB) {
		this.matrixB = matrixB;
	}

	public String getMatrixRowsAndColumnsSetting() {
		return matrixRowsAndColumnsSetting;
	}

	@SuppressWarnings("static-access")
	public void setMatrixRowsAndColumnsSetting(String matrixRowsAndColumnsSetting) {
		this.matrixRowsAndColumnsSetting = matrixRowsAndColumnsSetting;
	}

	public Object[][] getCalculationResult() {
		return calculationResult;
	}

	public void setCalculationResult(Object[][] calculationResult) {
		this.calculationResult = calculationResult;
	}

}
