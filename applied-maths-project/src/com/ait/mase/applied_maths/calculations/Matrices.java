package com.ait.mase.applied_maths.calculations;

import java.util.Random;

/**
 * The class {@code Matrices} is used to generate and calculate matrices
 * 
 * @author Team: Athena
 *
 */
public class Matrices {

	private static Random randomGenerator = new Random();

	/**
	 * Generate and print a basic random m x n matrix depending on the given
	 * rows and columns.
	 * 
	 * @param row
	 * @param column
	 * @param minRandimNum
	 * @param maxRandomNum
	 * @return a matrix
	 */
	public static Integer[][] generateBasicMatrix(int row, int column, int minRandomNum, int maxRandomNum) {

		Integer[][] matrix = new Integer[row][column];

		for (int r = 0; r < row; r++) {
			for (int c = 0; c < column; c++) {
				matrix[r][c] = randomGenerator.nextInt(maxRandomNum + 1 - minRandomNum) + minRandomNum;
			}
		}
		return matrix;
	}

	/**
	 * Method used to add, subtract, or divide two matrices
	 * 
	 * @param matrixA
	 * @param matrixB
	 * @param operationType
	 *            - Defines the operation type (i.e. if it should add, subtract,
	 *            or divide)
	 * @return result of the two matrices
	 */
	public static Object[][] addSubtractOrDivideMatrices(Integer[][] matrixA, Integer[][] matrixB,
			String operationType) {

		Integer[][] result = new Integer[matrixA[0].length][matrixA[1].length];

		Double[][] divisionResults = new Double[matrixA[0].length][matrixA[1].length];

		Double[][] matrixBInversed = new Double[matrixA[0].length][matrixA[1].length];

		double divisionSum = 0;

		if (operationType.equalsIgnoreCase("divide")) {
			matrixBInversed = getInversedMatrix(matrixB);
		}

		for (int r = 0; r < matrixA.length; r++) {
			for (int c = 0; c < matrixA[0].length; c++) {
				if (operationType.equalsIgnoreCase("add"))
					result[r][c] = matrixA[r][c] + matrixB[r][c];
				else if (operationType.equalsIgnoreCase("subtract"))
					result[r][c] = matrixA[r][c] - matrixB[r][c];
				else if (operationType.equalsIgnoreCase("divide")) {
					divisionSum = 0;
					for (int k = 0; k < matrixA[0].length; k++) {
						divisionSum += matrixA[r][k] * matrixBInversed[k][c];
					}
					divisionResults[r][c] = divisionSum;
				}
			}
		}

		if (operationType.equalsIgnoreCase("divide"))
			return divisionResults;
		else
			return result;
	}

	/**
	 * Method converts a matrix to an inversed matrix
	 * 
	 * @param matrix
	 * 
	 * @reference https://www.thejavaprogrammer.com/java-program-find-inverse-matrix/
	 * @return Inverse matrix
	 */
	private static Double[][] getInversedMatrix(Integer[][] matrix) {
		float det = 0;

		Double[][] inversedMatrix = new Double[matrix.length][matrix.length];

		for (int i = 0; i < matrix.length; i++) {
			det = det + (matrix[0][i] * (matrix[1][(i + 1) % 3] * matrix[2][(i + 2) % 3]
					- matrix[1][(i + 2) % 3] * matrix[2][(i + 1) % 3]));
		}

		for (int i = 0; i < matrix.length; ++i) {
			for (int j = 0; j < matrix.length; ++j) {
				inversedMatrix[i][j] = Double
						.parseDouble((((matrix[(j + 1) % 3][(i + 1) % 3] * matrix[(j + 2) % 3][(i + 2) % 3])
								- (matrix[(j + 1) % 3][(i + 2) % 3] * matrix[(j + 2) % 3][(i + 1) % 3])) / det) + "");
			}
		}

		return inversedMatrix;
	}

	/**
	 * Method used to multiply two matrices
	 * 
	 * @param matrixA
	 * @param matrixB
	 * @return
	 */
	public static Integer[][] multiplyMatrices(Integer[][] matrixA, Integer[][] matrixB) {
		Integer[][] result = new Integer[matrixA[0].length][matrixA[1].length];
		int sum = 0;
		for (int r = 0; r < matrixA[0].length; r++) {
			for (int c = 0; c < matrixA[0].length; c++) {
				sum = 0;
				for (int k = 0; k < matrixA[0].length; k++) {
					sum += matrixA[r][k] * matrixB[k][c];
				}
				result[r][c] = sum;
			}
		}
		return result;
	}

}
