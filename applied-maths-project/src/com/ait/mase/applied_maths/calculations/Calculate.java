package com.ait.mase.applied_maths.calculations;


/**
 * The class {@code Calculate} is used for basic mathematical calculations such
 * as logarithms, square roots, exponential etc.
 * 
 * @author Team: Athena
 *
 */
public class Calculate {

	/**
	 * Calculates the sum of two values
	 * 
	 * @param value
	 * @param value
	 * @return sum of the two values
	 */

	public static double additionOf(double value1, double value2) {
		double result = value1 + value2;
		return result;
	}

	/**
	 * Calculates the difference between two values
	 * 
	 * @param value
	 * @param value
	 * @return difference between two values
	 */

	public static double substractionOf(double value1, double value2) {
		double result = value1 - value2;
		return result;
	}

	/**
	 * Calculates the product of two values
	 * 
	 * @param value
	 * @param value
	 * @return product of the two values
	 */

	public static double productOf(double value1, double value2) {
		double result = value1 * value2;
		return result;
	}
	
	/**
	 * Calculates the quotient of two values
	 * 
	 * @param value
	 * @param value
	 * @return quotient of the two values
	 */

	public static double quotientOf(double value1, double value2) {
		double result = value1 / value2;
		return result;
	}
	
	
	/**
	 * Calculates the root to the nth degree
	 * 
	 * @param value
	 * @param root
	 * @return root to the nth degree
	 */
	public static double rootOf(double root, double value) {
		return Math.pow(Math.E, Math.log(value) / root);
	}

	/**
	 * Calculates the square root of a given value
	 * 
	 * @param value
	 * @return square root of a given value
	 */
	public static double sqrRootOf(double value) {
		return Math.sqrt(value);
	}

	/**
	 * Calculate the power of a given value
	 * 
	 * @param value
	 * @param power
	 * @return the power of a given value
	 */
	public static double powerOf(double value, double power) {
		return Math.pow(value, power);
	}

	/**
	 * Calculate the natural logarithm of a given value
	 * 
	 * @param l
	 * @return the logarithm of a given value
	 */
	public static double lnOf(double value) {
		return Math.log(value);
	}

	/**
	 * Calculates the base 10 logarithm of a given value
	 * 
	 * @param value
	 * @return base 10 logarithm
	 */
	public static double ln10Of(double value) {
		return Math.log10(value);
	}

	/**
	 * Calculate the exponential
	 * 
	 * @param exp
	 * @return exponential value
	 */
	public static double expOf(double exp) {
		return Math.exp(exp);
	}

	/**
	 * Calculate the sin (radians)
	 * 
	 * @param degrees
	 * @return exponential value
	 */
	public static double sinof(double degrees) {
		double radians = Math.toRadians(degrees);
		return Math.sin(radians);
	}

	/**
	 * Calculate the cos (radians)
	 * 
	 * @param degrees
	 * @return exponential value
	 */
	public static double cosof(double degrees) {
		double radians = Math.toRadians(degrees);
		return Math.cos(radians);
	}

	/**
	 * Calculate the tan (radians)
	 * 
	 * @param degrees
	 * @return exponential value
	 */
	public static double tanof(double degrees) {
		double radians = Math.toRadians(degrees);
		return Math.tan(radians);
	}
	
	/**
	 * Calculate the sin (degrees)
	 * 
	 * @param degrees
	 * @return exponential value
	 */
	public static double sinofDeg(double degrees) {
		return Math.sin(degrees);
	}

	/**
	 * Calculate the cos (degrees)
	 * 
	 * @param degrees
	 * @return exponential value
	 */
	public static double cosofDeg(double degrees) {
		return Math.cos(degrees);
	}

	/**
	 * Calculate the tan (degrees)
	 * 
	 * @param degrees
	 * @return exponential value
	 */
	public static double tanofDeg(double degrees) {
		return Math.tan(degrees);
	}
	
	/**
	 * Rounds a given value to a desired decimal place
	 * 
	 * @param value
	 * @param places
	 * @return rounded value
	 */
	public static Double round(double value, int places) {
		try {
			long factor = (long) Math.pow(10, places);
			value = value * factor;
			long tmp = Math.round(value);
			return (double) tmp / factor;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
	public static double calculateRadian(double value){		
		double result = Math.toRadians(value);
		return result;
	}
	
	/**
	 * Calculates the factorial of a number.
	 * 
	 * @param number to find factorial of.
	 * @return factorial.
	 */
	public static double factorial(double number) {
		
		int fact = 1;
		
		for(int i=1;i<=number;i++){    
		      fact=fact*i;    
		  }  
		
		return fact;
		
	}
	
	/**
	 * Finds the probability of a certain event occurring.
	 * 
	 * @param possible outcomes
	 * @param possitive outcomes
	 * @return probability of combination
	 */
	public static double probability(double possible, double positive) {
				
		return possible/positive;
	}
	
	/**
	 * Finds the probability of a combination occurring. - HAS ISSUES WITH LONG NUMBERS
	 * 
	 * @param num of white balls
	 * @param num of black balls
	 * @param n - number of the type of balls being selected
	 * @param r - number of balls to be selected
	 * @return probability
	 */
	public static double combination(double white, double black, double n, double r) {
				
		double total = white + black;
		if(r > n || r < 0) {
			return 0;
		}
		
		else {
			
			double combination = (factorial(n)/(factorial(r) * factorial(n-r)))/(factorial(total)/(factorial(r)*factorial(total - r)));
			return combination;
		}
	}
	
}
