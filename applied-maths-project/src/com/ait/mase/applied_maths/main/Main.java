package com.ait.mase.applied_maths.main;

import com.ait.mase.applied_maths.calculations.Calculate;
import com.ait.mase.applied_maths.calculations.Matrices;

/**
 * Main Java class used to create instances of other classes and execute methods
 * @author Team: Athena
 *
 */
public class Main {

	public static void main(String[] args) {
		
		System.out.println("Sum of 10 and 30 = " + Calculate.additionOf(10,30));
		System.out.println("Subtraction of 40 and 10 = " + Calculate.substractionOf(40,10));
		System.out.println("Product of 10 and 20 = " + Calculate.productOf(10,20));
		System.out.println("Quotient of 40 and 5 = " + Calculate.quotientOf(40,5));
		System.out.println("Root 5 of 100 = " + Calculate.rootOf(5, 100));
		System.out.println("Square Root of 10 = " + Calculate.sqrRootOf(10));
		System.out.println("10^5 =  " + Calculate.powerOf(10, 5));
		System.out.println("log(8) = " + Calculate.lnOf(8));
		System.out.println("log10(8) = " + Calculate.ln10Of(8));
		System.out.println("exp e^x 5 = " + Calculate.expOf(5));
		System.out.println("sin(90) = " + Calculate.sinof(90));
		System.out.println("cos(45) = " + Calculate.cosof(45));
		System.out.println("tan(50) = " + Calculate.tanof(50) + "\n");
		System.out.println(Matrices.generateBasicMatrix(4, 3, 10, 12));
		
	}
}
