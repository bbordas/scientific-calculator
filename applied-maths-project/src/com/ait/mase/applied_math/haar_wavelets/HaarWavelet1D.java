package com.ait.mase.applied_math.haar_wavelets;

import java.io.IOException;

public class HaarWavelet1D {

	double[] matrix1D;
	int size;

	//A 1D wavelet transform is applied to each row of pixels, generating an 
	//average value and difference values for each row.  
	//These transformed rows are treated as a new signal
	
	public static void main(String[] args) throws IOException {
		// 1D Haar Matrix - 4 pixels
		double[] data1D = {8, 9, 2, 4};
		//instantiate class to transform the matrix 
		HaarWavelet1D Haar1D = new HaarWavelet1D(data1D);
		System.out.println("Processing 1D Haar Wavelet Transformation");
		System.out.println("Input:");
		Haar1D.output();
		Haar1D.Decomposition();
		System.out.println();
		System.out.println("Output:");
		Haar1D.output();
		System.out.println();
	}

	public HaarWavelet1D(double[] data) {
		matrix1D = data;
		size = matrix1D.length;
	}

	public void Decomposition() {
		int steps = size;
		double[] row = matrix1D;
		while (steps > 1) {
			row = decompositionStep(steps, row);
			steps = steps / 2;
		}
		matrix1D = row;
	}

	public double[] decompositionStep(int step, double[] row) {
		double[] newRow = new double[row.length];
		System.arraycopy( row, 0, newRow, 0, row.length );
		for (int i=0 ; i < step/2 ; i++) {
			newRow[i] = (row[2*i + 1] + row[2*i])/ 2;
			newRow[step/2 + i] = (row[(2*i)] - row[(2*i + 1)])/2;	
		}
		return newRow;
	}

	public void output() {
		for (int i = 0; i < size; i++) {
			System.out.print(matrix1D[i] + ", ");
		}
		System.out.println("");
	}
}