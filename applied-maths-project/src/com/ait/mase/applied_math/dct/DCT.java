package com.ait.mase.applied_math.dct;

import java.text.DecimalFormat;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
@ManagedBean
@RequestScoped
public class DCT
{
    private DecimalFormat df = new DecimalFormat("####0.00");
    private int N = 8;
    private double n = 8.0;
    private double [][]TM = new double[N][N];
    private double [][]DCT = new double[N][N];
    private double [][]R = new double[N][N];
    private Integer [][]C;
    public static boolean showResults;   
     
    public DCT() {
		super();
	}

	public Integer[][] getC() {
		return C;
	}

	public void setC(Integer[][] c) {
		C = c;
	}
	
	
    public boolean isShowResults() {
		return showResults;
	}

	public void setShowResults(boolean showResults) {
		this.showResults = showResults;
	}

	private int Qlevel;
    private int[][] Q = new int[8][8];
    //private double[][] R = new double[8][8];
    private int[][] Q50 = {{16,11,10,16,24,40,51,61},
            {12,12,14,19,26,58,60,55},
            {14,13,16,24,40,57,69,56},
            {14,17,22,29,51,87,80,62},
            {18,22,37,56,68,109,103,77},
            {24,35,55,64,81,104,113,92},
            {49,64,78,87,103,121,120,101},
            {72,92,95,98,112,100,103,99}};

    private Integer[][] M = new Integer[N][N];
    
    private double[][] T = new double[N][N];
    private double[][] Tt = new double[N][N];
    public DCT(int level, Integer[][] m_Matrix)
    {
        Qlevel = level;
        M = m_Matrix;
        Generate_T();
        CalculateQ(Qlevel);
    }
    public void QuantizationStepForward()
    {
    	 C = new Integer[N][N];
        for(int r=0;r<N;r++)
            for(int c=0;c<N; c++)
                C[r][c] = (int)Math.round(DCT[r][c]/Q[r][c]);
    }
    public void QuantizationStepInverse()
    {
        for(int r=0;r<N;r++)
            for(int c=0;c<N; c++)
                R[r][c] = (int)Math.round(C[r][c]*Q[r][c]);
    }
    public void Calculate_FDCT()
    {
        double sum = 0.0;
        for(int r=0;r<N;r++){
            for(int c=0;c<N; c++){
                for(int k=0;k<N; k++)
                    sum = sum + T[r][k]*M[k][c];
                TM[r][c] = sum;
                sum = 0;
            }
        }
        sum=0.0;
        for(int r=0;r<N;r++){
            for(int c=0;c<N; c++){
                for(int k=0;k<N; k++)
                    sum = sum + TM[r][k]*Tt[k][c];
                DCT[r][c] = sum;
                sum = 0;
            }
        }
    }
    public Integer[][] Calculate_IDCT()
    {
        double sum = 0.0;
        for(int r=0;r<N;r++){
            for(int c=0;c<N; c++){
                for(int k=0;k<N; k++)
                    sum = sum + Tt[r][k]*R[k][c];
                TM[r][c] = sum;
                sum = 0;
            }
        }
        sum=0.0;
        for(int r=0;r<N;r++){
            for(int c=0;c<N; c++){
                for(int k=0;k<N; k++)
                    sum = sum + TM[r][k]*T[k][c];
                M[r][c] =(int) sum;
                sum = 0;
            }
        }
        return M;
    }

    public void Generate_T(){
         for(int r=0;r<N;r++){
            for(int c=0;c<N; c++){
               if(r==0)
                   T[r][c] = 1/Math.sqrt(n);
               else
                   T[r][c] = Math.sqrt((2.0/n))*Math.cos((r*Math.PI*(2*c+1))/(2*n));
               Tt[c][r] = T[r][c];
            }
        }
    }
    
    private void CalculateQ(int q){
        int temp=0;
        System.out.println("Quality Selected: "+q);
        if(q==50)
            for(int n=0; n<8; n++)
                System.arraycopy(Q50[n], 0, Q[n], 0, 8);
        
        else if(q>=1 && q<50)
            for(int r=0;r<8;r++){
                for(int c=0;c<8; c++){
                    temp = (int)(Q50[r][c]*(50/q));
                    if(temp>255)
                        Q[r][c] = 255;
                    else
                        Q[r][c] = (int)Math.round(Q50[r][c]*(50/(double)q));
                }
            }
        else if(q>50 && q<=100)
            for(int r=0;r<8;r++)
                for(int c=0;c<8; c++)
                    Q[r][c] = (int)Math.round(Q50[r][c]*((100-(double)q)/50));
        else
            System.out.println("Invalid number");
        printArray("Quantization Matrix", Q);
    }
    public void printArray(int a){
        if(a==1){
            for(int r=0;r<N;r++){
                for(int c=0;c<N; c++){
                   System.out.print((int)Math.round(C[r][c])+" ") ;
                }  System.out.println(" ") ;
            }           
        }
        else if(a==2){
            for(int r=0;r<N;r++){
                for(int c=0;c<N; c++){
                   System.out.print((int)Math.round(R[r][c])+" ") ;
                }  System.out.println(" ") ;
            }
        }
        else if(a==3){
            for(int r=0;r<N;r++){
                for(int c=0;c<N; c++){
                   System.out.print((M[r][c])+" ") ;
                }  System.out.println(" ") ;
            }
        }
        else
            System.out.println("Something Went wrong.....");

    }
    public void printArray(String s, int[][] m){
        System.out.println(s);
        for(int r=0;r<N;r++){
            for(int c=0;c<N; c++)
            {
               System.out.print((int)Math.round(m[r][c])+" ") ;
            }  System.out.println(" ") ;
        }
    }
    
    public void printArray(String s, Integer[][] m){
        System.out.println(s);
        for(int r=0;r<N;r++){
            for(int c=0;c<N; c++)
            {
               System.out.print((int)Math.round(m[r][c])+" ") ;
            }  System.out.println(" ") ;
        }
    }
 
    
}
