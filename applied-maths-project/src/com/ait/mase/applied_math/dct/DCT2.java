package com.ait.mase.applied_math.dct;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Scanner;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class DCT2 {
	
	public  Scanner myScanner = new Scanner(System.in);
	public static int  quantizationLevel = 0;
	int n = 8;
	Object[][] q_matrix = new Object[n][n];
	public Object[][] T = new Object[n][n];
	public Object[][] Tt = new Object[n][n];
	public Object[][] TM = new Object[n][n];
	public Object[][] D = new Object[n][n];
	public Object[][] C = new Object[n][n];
	
	//create an object of this class then use getters to build the array
	public Object[][] M = new Object[8][8];
	
	 NumberFormat formatter = new DecimalFormat("#0.00"); 
	
	// getters & setters	
	public  Scanner getMyScanner() {
		return myScanner;
	}	

	public static void setMyScanner(Scanner myScanner) {
		myScanner = myScanner;
	}

	public int getQuantizationLevel() {
		return quantizationLevel;
	}

	public void setQuantizationLevel(int quantizationLevel) {
		this.quantizationLevel = quantizationLevel;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		n = n;
	}
	
	public Object[][] getQ_matrix() {
		return q_matrix;
	}

	public void setQ_matrix(Object[][] q_matrix) {
		this.q_matrix = q_matrix;
	}

	public Object[][] getT() {
		return T;
	}

	public void setT(Object[][] t) {
		T = t;
	}

	public Object[][] getTt() {
		return Tt;
	}

	public void setTt(Object[][] tt) {
		Tt = tt;
	}

	public Object[][] getTM() {
		return TM;
	}

	public void setTM(Object[][] tM) {
		TM = tM;
	}

	public Object[][] getD() {
		return D;
	}

	public  void setD(Object[][] d) {
		D = d;
	}

	public  Object[][] getC() {
		return C;
	}

	public  void setC(Object[][] c) {
		C = c;
	}

	public Object[][] getM() {
		return M;
	}
	
	public void setM(Object[][] m) {
		M = m;
	}

	public  NumberFormat getFormatter() {
		return formatter;
	}

	public  void setFormatter(NumberFormat formatter) {
		formatter = formatter;
	}
	

	// methods
	public  void generate_T() {
		for (int row=0; row<n; row++) {
			for (int column=0; column<n; column++) {
				if(row==0) {
					T[row][column] = 1/Math.sqrt(n);
				} else {
					T[row][column] = Math.sqrt((2.0/n))*Math.cos((row*Math.PI*(2*column+1))/(2*n));
				}
			}
		}
	}

	public  void transpose_T() {
		for (int row=0; row<n; row++) {
			for (int column=0; column<n; column++) {
				Tt[row][column] = T[column][row];
			}
		}
	}
	
	public  void generate_TM() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int temp = 0;
                for (int k = 0; k < n; k++) {
                    temp += (((Integer)T[i][k]) * ((Integer)M[k][j]));
                }
                TM[i][j] = Integer.valueOf(formatter.format(temp));
            }
        }
	}
	
	public  void generate_TMT() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int temp = 0;
                for (int k = 0; k < n; k++) {
                    temp += (((Integer)TM[i][k]) * ((Integer)Tt[k][j]));
                }
                D[i][j] = Integer.valueOf(formatter.format(temp));
            }
        }
	}
	
	public  void quantization() {
		q_matrix = get_Quantization_Matrix(this.quantizationLevel); print(q_matrix);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int temp = (int) Math.round(((Integer)D[i][j]) / ((Integer)q_matrix[i][j]));
                C[i][j] = Integer.valueOf(formatter.format(temp));
            }
        }		
	}
	
	public  void print(Object[][] input) {
		for(int i=0; i<input.length; i++) {
			for(int j=0; j<input[i].length; j++){
				System.out.print(formatter.format(input[i][j]) + " ");
			}
			System.out.println();
		}	
		System.out.println();
	}
	
	public Object[][] get_Quantization_Matrix(int level) {
		Object[][] quantMatix = new Object[8][8];
		Object[][] Q50_Matrix = { {16, 11, 10, 16, 24, 40, 51, 61}, 
					   		   {12, 12, 14, 19, 26, 58, 60, 55},
					   		   {14, 13, 16, 24, 40, 57, 69, 56}, 
					   		   {14, 17, 22, 29, 51, 87, 80, 62},
					   		   {18, 22, 37, 56, 68, 109, 103, 77},
					   		   {24, 35, 55, 64, 81, 104, 113, 92},
					   		   {49, 64, 78, 87, 103, 121, 120, 101},
					   		   {72, 92, 95, 98, 112, 100, 103, 99} };
		
		
		System.out.println("Quant Level is : " + level);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
            	if(level>50) {
            		int temp = Math.round(((Integer)Q50_Matrix[i][j])*(100-level)/50);
            		quantMatix[i][j] = Integer.valueOf(formatter.format(temp));
            	} else if(level < 50) {
            		int temp = Math.round(((Integer)Q50_Matrix[i][j])*(50/level));
            		quantMatix[i][j] = Integer.valueOf(formatter.format(temp));
            	} else {
            		quantMatix = Q50_Matrix;
            	}
            }
        }        
		return quantMatix;
	}
	
	public Object[][] compress(Object[][] theMatrix) {
		M = theMatrix;
		generate_T();
		transpose_T(); print(Tt);
		generate_TM(); print(TM);
		generate_TMT(); print(D);
		System.out.println("here");
		quantization(); 
		System.out.println("here");
		System.out.println("======================================");
		System.out.println("          Compressed Block");
		System.out.println("======================================");
		print(C);
		return M;
	}
	

/*	public static void main(String[] args) {
		new DCT().compress();
	}*/

}

/*input = { 
	{154, 123, 123, 123, 123, 123, 123, 136},
	{192, 180, 136, 154, 184, 154, 136, 110},
	{254, 198, 154, 154, 180, 154, 123, 123},
	{239, 180, 136, 180, 180, 166, 123, 123},
	{180, 154, 136, 167, 166, 149, 136, 136},
	{128, 136, 123, 136, 154, 180, 198, 154},
	{123, 105, 110, 149, 136, 136, 180, 166},
	{110, 136, 123, 123, 123, 136, 154, 136} };
	
	
	
input = {{17.0, 7.0, 19.0, 18.0, 6.0, 10.0, 14.0, 18},
								{21.0, 20.0, 20.0, 11.0, 12.0, 21.0, 14.0, 11.0},
								{15.0, 19.0, 27.0, 18.0, 7.0, 12.0, 16.0, 14.0},
								{15.0, 7.0, 11.0, 15.0, 11.0, 12.0, 15.0, 13.0},
								{8.0, 11.0, 7.0, 9.0, 16.0, 13.0, 10.0,16.0},
								{8.0, 23.0, 19.0, 12.0, 11.0, 11.0, 16.0, 18.0},
								{23.0, 25.0, 16.0, 14.0, 16.0, 20.0, 24.0, 12.0},
								{21.0, 29.0, 16.0, 16.0, 21.0, 13.0, 17.0, 25.0}};
*/