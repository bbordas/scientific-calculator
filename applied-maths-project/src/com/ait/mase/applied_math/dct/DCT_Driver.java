package com.ait.mase.applied_math.dct;

import java.util.Scanner;

public class DCT_Driver{
	DCT run;
	
    public Integer[][] compress(int qLevel, Integer[][] theM_Matix) {
    	run = new DCT(qLevel, theM_Matix); 
    	//System.out.println("\n\nMessage to be compressed:");
        //run.printArray(3);
    	run.printArray("\n\nMessage to be compressed:", theM_Matix);
        run.Calculate_FDCT();
        run.QuantizationStepForward();
        System.out.println("\nCompressed Message");
        run.printArray(1);
        System.out.println("\nDecompressed Message");
        run.QuantizationStepInverse();
        run.Calculate_IDCT();
        run.printArray(3);
        DCT.showResults=true;
        System.out.println(DCT.showResults);
        return run.Calculate_IDCT();
    }
    
    public Integer[][] getCMatrix() {
    	return run.getC();
    }
}
