package com.ait.mase.applied_maths.models;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SquareRootTest {

	private SquareRoot sqrt;

	@Before
	public void setup(){
		sqrt = new SquareRoot();
	}

	@Test
	public void testDefaultConstructor() {
		assertThat(sqrt, instanceOf(SquareRoot.class));
	}
	
	@Test
	public void testGettersAndSetters(){
		sqrt.setValue(10);
		sqrt.setAnswer(12);
		assertEquals(10, sqrt.getValue(), 0);
		assertEquals(12, sqrt.getAnswer(), 0);
	}

}
