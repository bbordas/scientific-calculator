package com.ait.mase.applied_maths.models;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.instanceOf;


public class SinCosTanTest {
	
	private SinCosTan sct;

	@Before
	public void setup(){
		sct = new SinCosTan();
	}

	@Test
	public void testDefaultConstructor() {
		assertThat(sct, instanceOf(SinCosTan.class));
	}
	
	@Test
	public void testGettersAndSetters(){
		sct.setValue(180);
		sct.setBaseType("sin");
		sct.setAnswer(0);
		assertEquals(180, sct.getValue(), 0);
		assertEquals("sin", sct.getBaseType());
		assertEquals(0, sct.getAnswer(), 0);
	}
}