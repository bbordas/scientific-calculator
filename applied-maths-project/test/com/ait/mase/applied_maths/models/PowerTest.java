package com.ait.mase.applied_maths.models;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PowerTest {
	
	private Power power;

	@Before
	public void setup(){
		power = new Power();
	}

	@Test
	public void testDefaultConstructor() {
		assertThat(power, instanceOf(Power.class));
	}
	
	@Test
	public void testGettersAndSetters(){
		power.setValue(10);
		power.setPower(2);
		power.setAnswer(12);
		assertEquals(10, power.getValue(), 0);
		assertEquals(2, power.getPower(), 0);
		assertEquals(12, power.getAnswer(), 0);
	}

}
