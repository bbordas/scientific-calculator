package com.ait.mase.applied_maths.models;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.instanceOf;

public class LogarithmTest {
	
	private Logarithm log;

	@Before
	public void setup(){
		log = new Logarithm();
	}
	
	@Test
	public void testDefaultConstructor() {
		assertThat(log, instanceOf(Logarithm.class));
	}
	
	@Test
	public void testGettersAndSetters(){
		log.setValue(10);
		log.setBaseType("natural");
		log.setAnswer(12);
		assertEquals(10, log.getValue(), 0);
		assertEquals("natural", log.getBaseType());
		assertEquals(12, log.getAnswer(), 0);
	}

}
