package com.ait.mase.applied_maths.models;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SetsModelTest {

	private static SetsModel sm;
	@Before
	public void setup(){
		sm = new SetsModel();
	}

	@Test
	public void testDefaultConstructor() {
		assertThat(sm, instanceOf(SetsModel.class));
	}
	
	@Test
	public void testGettersAndSetters(){
		sm.setInput("1 2 3");
		sm.setOutPut("2");
		assertEquals("1 2 3", sm.getInput(), "1 2 3");
		assertEquals("2", sm.getOutPut(), "2");

	}

}
