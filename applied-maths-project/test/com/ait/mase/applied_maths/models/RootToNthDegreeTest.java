package com.ait.mase.applied_maths.models;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RootToNthDegreeTest {

	private RootToNthDegree root;

	@Before
	public void setup(){
		root = new RootToNthDegree();
	}

	@Test
	public void testDefaultConstructor() {
		assertThat(root, instanceOf(RootToNthDegree.class));
	}
	
	@Test
	public void testGettersAndSetters(){
		root.setRootValue(10);
		root.setOfValue(2);
		root.setAnswer(12);
		assertEquals(10, root.getRootValue(), 0);
		assertEquals(2, root.getOfValue(), 0);
		assertEquals(12, root.getAnswer(), 0);
	}

}
