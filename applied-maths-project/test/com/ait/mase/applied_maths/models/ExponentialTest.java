package com.ait.mase.applied_maths.models;


import static org.junit.Assert.*;

import org.junit.Before;

import static org.hamcrest.CoreMatchers.instanceOf;
import org.junit.Test;

public class ExponentialTest {
	
	private Exponential exp;
	
	@Before
	public void setup(){
		exp = new Exponential();
	}

	@Test
	public void testDefaultConstructor() {
		assertThat(exp, instanceOf(Exponential.class));
	}
	
	@Test
	public void testGettersAndSetters(){
		exp.setValue(10);
		exp.setAnswer(12);
		assertEquals(10, exp.getValue(), 0);
		assertEquals(12, exp.getAnswer(), 0);
		
	}

}
