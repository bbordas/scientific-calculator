package com.ait.mase.applied_maths.beans;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.primefaces.model.chart.PieChartModel;

public class PieChartBeanTest {

	PieChartBean pieChart;

	@Before
	public void setup() {
		pieChart = new PieChartBean();
	}

	@Test
	public void testInit() {
		PieChartModel pcm = new PieChartModel();
		pcm.setTitle("Pie Chart - Percentage");
		pcm.setLegendPosition("w");
		pcm.setFill(true);
		pcm.setShowDataLabels(true);
		pcm.setDiameter(200);

		pieChart.init(); // Create the default pie chart by using init()

		assertEquals(pcm.getTitle(), pieChart.getPieModel2().getTitle());
		assertEquals(pcm.getLegendPosition(), pieChart.getPieModel2().getLegendPosition());
		assertEquals(pcm.isFill(), pieChart.getPieModel2().isFill());
		assertEquals(pcm.isShowDataLabels(), pieChart.getPieModel2().isShowDataLabels());
		assertEquals(pcm.getDiameter(), pieChart.getPieModel2().getDiameter());

	}

	@Test
	public void testCreatePieModel2() {
		PieChartModel pcm = new PieChartModel();
		pcm.setTitle("Pie Chart - Percentage");
		pcm.setLegendPosition("w");
		pcm.setFill(true);
		pcm.setShowDataLabels(true);
		pcm.setDiameter(200);

		pieChart.createPieModel2();

		assertEquals(pcm.getTitle(), pieChart.getPieModel2().getTitle());
		assertEquals(pcm.getLegendPosition(), pieChart.getPieModel2().getLegendPosition());
		assertEquals(pcm.isFill(), pieChart.getPieModel2().isFill());
		assertEquals(pcm.isShowDataLabels(), pieChart.getPieModel2().isShowDataLabels());
		assertEquals(pcm.getDiameter(), pieChart.getPieModel2().getDiameter());
	}

	@Test
	public void testCreatePieModel3Success() {
		PieChartModel pcm = new PieChartModel();
		pcm.setTitle("Pie Chart - Percentage");
		pcm.setLegendPosition("w");
		pcm.setShowDataLabels(true);
		pcm.setDiameter(200);
		String pieValues = "1,2,3, 4, 5 , 6";

		pieChart.setPieTextBoxValue("1,2,3, 4, 5 , 6");
		pieChart.createPieModel3();

		assertEquals(pieValues, pieChart.getPieTextBoxValue());
		assertEquals(pcm.getTitle(), pieChart.getPieModel2().getTitle());
		assertEquals(pcm.getLegendPosition(), pieChart.getPieModel2().getLegendPosition());
		assertEquals(pcm.isFill(), pieChart.getPieModel2().isFill());
		assertEquals(pcm.isShowDataLabels(), pieChart.getPieModel2().isShowDataLabels());
		assertEquals(pcm.getDiameter(), pieChart.getPieModel2().getDiameter());
	}

	@Test
	public void testCreatePieModel3Error() {
		PieChartModel pcm = new PieChartModel();
		pcm.set("Error: Invalid Data Format", 1);
		pcm.setShowDataLabels(false);
		pcm.setLegendPosition("w");
		pcm.setDiameter(200);
		pcm.setTitle("An Error Occurred - Invalid Data Format");
		int average = 0;
		int total = 0;
		int min = 0;
		int max = 0;
		String pieValues = "a,b, c d";

		pieChart.setPieTextBoxValue("a,b, c d");
		pieChart.createPieModel3();

		assertEquals(pieValues, pieChart.getPieTextBoxValue());
		assertEquals(pcm.getTitle(), pieChart.getPieModel2().getTitle());
		assertEquals(pcm.getLegendPosition(), pieChart.getPieModel2().getLegendPosition());
		assertEquals(pcm.isFill(), pieChart.getPieModel2().isFill());
		assertEquals(pcm.isShowDataLabels(), pieChart.getPieModel2().isShowDataLabels());
		assertEquals(pcm.getDiameter(), pieChart.getPieModel2().getDiameter());
		assertEquals(average, pieChart.getAverage(), 0);
		assertEquals(total, pieChart.getTotal(), 0);
		assertEquals(min, pieChart.getMin(), 0);
		assertEquals(max, pieChart.getMax(), 0);
	}

	@Test
	public void testGettersAndSetters() {
		pieChart.setPieTextBoxValue("1,2,3");
		pieChart.setAverage(8);
		pieChart.setTotal(10);
		pieChart.setMax(5);
		pieChart.setMin(1);

		assertEquals("1,2,3", pieChart.getPieTextBoxValue());
		assertEquals(8, pieChart.getAverage(), 0);
		assertEquals(10, pieChart.getTotal(), 0);
		assertEquals(5, pieChart.getMax(), 0);
		assertEquals(1, pieChart.getMin(), 0);
	}

}
