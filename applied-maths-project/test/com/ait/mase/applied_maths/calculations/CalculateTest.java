package com.ait.mase.applied_maths.calculations;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.instanceOf;

import org.junit.Test;

public class CalculateTest {
	
	@Test
	public void testClassInstance(){
		Calculate calc = new Calculate();
		assertThat(calc, instanceOf(Calculate.class));
	}

	@Test
	public void testAddition() {
		assertEquals(10, Calculate.additionOf(5, 5), 0);
	}
	
	@Test
	public void testAdditionError() {
		assertEquals(10, Calculate.additionOf(5, 5), 0);
	}

	@Test
	public void testSubtraction() {
		assertEquals(20, Calculate.substractionOf(25, 5), 0);
	}

	@Test
	public void testRoot() {
		double expectedValue = Math.pow(Math.E, Math.log(5) / 3);
		assertEquals(expectedValue, Calculate.rootOf(3, 5), 0);
	}

	@Test
	public void testSquareRoot() {
		double expectedValue = Math.sqrt(10);
		assertEquals(expectedValue, Calculate.sqrRootOf(10), 0);
	}

	@Test
	public void testPower() {
		double expectedValue = Math.pow(2, 4);
		assertEquals(expectedValue, Calculate.powerOf(2, 4), 0);
	}

	@Test
	public void testLogarithm() {
		double expectedValue = Math.log(10);
		assertEquals(expectedValue, Calculate.lnOf(10), 0);
	}

	@Test
	public void testLogarithmBase10() {
		double expectedValue = Math.log10(5);
		assertEquals(expectedValue, Calculate.ln10Of(5), 0);
	}

	@Test
	public void testExponential() {
		double expectedValue = Math.exp(8);
		assertEquals(expectedValue, Calculate.expOf(8), 0);
	}
	
	//double radians = Math.toRadians(degrees);
	//return Math.sin(radians);
	
	@Test
	public void testSin() {
		double radians = Math.toRadians(8);
		double expectedValue = Math.sin(radians);
		assertEquals(expectedValue, Calculate.sinof(8), 0);
	}
	
	@Test
	public void testCos() {
		double radians = Math.toRadians(5);
		double expectedValue = Math.cos(radians);
		assertEquals(expectedValue, Calculate.cosof(5), 0);
	}
	
	@Test
	public void testTan() {
		double radians = Math.toRadians(9);
		double expectedValue = Math.tan(radians);
		assertEquals(expectedValue, Calculate.tanof(9), 0);
	}

}
