package com.ait.mase.applied_maths.calculations;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import com.ait.applied_maths.sets.Sets;
import com.ait.mase.applied_maths.beans.SetBean;
import com.ait.mase.applied_maths.models.SetsModel;

public class SetsTest {

	private SetBean sb;
	private SetsModel sm;
	private Sets set;

	@Before
	public void setUp() {
		sb = new SetBean();
		sm = new SetsModel();
		set = new Sets();
	}
	
	@Test
	public void testIfAdded() {
		ArrayList<Integer> inputInt = new ArrayList<>();
		inputInt.add(1);
		inputInt.add(2);
		inputInt.add(3);
		assertEquals("1 2 3", set.addItemsToList("1 2 3"), inputInt);
		assertEquals("3 items", 3, set.addItemsToList("1 2 3").size());
	}
	
	@Test
	public void testIfSorted() {	
		ArrayList<Integer> inputInt = new ArrayList<>();
		inputInt.add(1);
		inputInt.add(2);
		inputInt.add(3);
		assertEquals("1 2 3", inputInt, set.sorted(set.addItemsToList("2 1 3")));
		assertEquals("3 items", 3, set.addItemsToList("1 2 3").size());
	}
	
	@Test
	public void testIfFindDuplicates() {	
		HashSet<Integer> inputInt = new HashSet<>();
		inputInt.add(2);
		assertEquals(inputInt, set.findDuplicates(set.addItemsToList("1 2 2 2 3")));
	}
	
	@Test
	public void testIfFindUniques() {
		ArrayList<Integer> uniques;
		uniques = set.findUniques(set.addItemsToList("1 2 2 2 3"), set.findDuplicates(set.addItemsToList("1 2 2 2 3")));
		assertEquals(set.addItemsToList("1 3"),uniques);
		
	}
	
	@Test
	public void testTotal() {
		ArrayList<Integer> list  = set.addItemsToList("1 2 2 2 3");
		Integer total = 10;
		assertEquals(total, set.total(list));	
	}

	@Test
	public void testMean() {
		ArrayList<Integer> list  = set.addItemsToList("1 2 2 2 2 3");
		Double avg = 2.0;
		assertEquals(avg, set.mean(list));	
	}
	
	@Test
	public void testMedian() {
		ArrayList<Integer> list  = set.addItemsToList("1 3 4");
		Double median = 3.0;
		assertEquals(median, set.median(list));	
		list.add(5);
		median = 3.5; 
		assertEquals(median, set.median(list));	
		
	}
	
	@Test
	public void testRange() {
		ArrayList<Integer> list  = set.addItemsToList("1 3 4");
		assertEquals("Range 1 - 4", set.range(list));	
	}
	
	@Test
	public void testMode() {
		ArrayList<Integer> list  = set.addItemsToList("1 3 3 4 4 4");
		assertEquals("Value: 4 Frequency: 3", set.mode(list));	
	}
	
	@Test
	public void testWriteOut() {
		ArrayList<Integer> list  = set.addItemsToList("1 2 3");
		assertEquals("1 2 3 ", set.writeOut(list));	
	}
	
}
