
##### BACKLOG - https://easybacklog.com/accounts/23394/backlogs/260981 #####

# Aram
(^), roots (to the nth degree), ln, exp and log
Matric operations
Constants must be hard code into application and represented using symbols, i.e. π = 3.141592654, e = 2.718281828

# Shane
Sin, cos, tan, etc. of a function powers

# Bogi
Calculate basic mathematical operations (+, -, /, *) and specify whether the result is to be represented in rad or deg’s

# David
Shunting Yard
DST (demonstrate data compression (lossy and lossless))

# Conor
Graph any function between given ranges. Input a Fourier Series Transform and plot the resulting waveform


# Non-functional Requirement
A graphical user interface that allows for command line input

################################################################


##### NEXT WEEK 02/OCTOBER/2017 #####

# Shane
Add Sin, cos, tan, etc. of a function powers to interface

# Bogi
(+, -, /, *) to interface

# Aram
More matrices + * / -

# David
Complete + Error checking/handling
	- Shunting Yard
	- DST (demonstrate data compression (lossy and lossless))
	
# Conor
Matrices (identity matrix compined with existing standard matrix)
